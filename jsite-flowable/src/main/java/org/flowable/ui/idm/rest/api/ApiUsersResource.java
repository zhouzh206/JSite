/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.flowable.ui.idm.rest.api;

import org.flowable.idm.api.Group;
import org.flowable.idm.api.User;
import org.flowable.ui.common.model.GroupRepresentation;
import org.flowable.ui.common.model.UserRepresentation;
import org.flowable.ui.common.service.exception.NotFoundException;
import org.flowable.ui.idm.model.UserInformation;
import org.flowable.ui.idm.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class ApiUsersResource {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    protected UserService userService;

    @RequestMapping(value = "/idm/users/{userId}", method = RequestMethod.GET, produces = {"application/json"})
    public UserRepresentation getUserInformation(@PathVariable String userId) {
        UserInformation userInformation = userService.getUserInformation(userId);
        if (userInformation != null) {
            UserRepresentation userRepresentation = new UserRepresentation(userInformation.getUser());
            if (userInformation.getGroups() != null) {
                for (Group group : userInformation.getGroups()) {
                    userRepresentation.getGroups().add(new GroupRepresentation(group));
                }
            }
            if (userInformation.getPrivileges() != null) {
                for (String privilege : userInformation.getPrivileges()) {
                    userRepresentation.getPrivileges().add(privilege);
                }
            }
            return userRepresentation;
        } else {
            throw new NotFoundException();
        }
    }

    @RequestMapping(value = "/idm/users", method = RequestMethod.GET, produces = {"application/json"})
    public List<UserRepresentation> findUsersByFilter(@RequestParam("filter") String filter, HttpServletResponse response) {
        // HttpClient 转发 过来的，eg. HttpClientUtils.httpGet("http://39.105.173.191:5555/jsite/api/idm/users?filter=xxx");
        // 存在跨域问题 所以增加下面的设置  参考：https://blog.csdn.net/qq_41463655/article/details/89703831
        response.setHeader("Access-Control-Allow-Origin", "*");
        logger.debug("-------------------findUsersByFilter------------------------");
        List<User> users = userService.getUsers(filter, null, null);
        List<UserRepresentation> result = new ArrayList<>();
        for (User user : users) {
            result.add(new UserRepresentation(user));
        }
        return result;
    }

}
