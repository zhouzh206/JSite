/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.web;

import com.jsite.common.config.Global;
import com.jsite.common.lang.StringUtils;
import com.jsite.common.persistence.Page;
import com.jsite.common.web.BaseController;
import com.jsite.modules.flowable.entity.FormData;
import com.jsite.modules.flowable.service.FormDataService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 表单数据表生成Controller
 * @author liuruijun
 * @version 2019-04-01
 */
@Controller
@RequestMapping(value = "${adminPath}/flowable/formData")
public class FormDataController extends BaseController {

	@Autowired
	private FormDataService formDataService;
	
	@ModelAttribute
	public FormData get(@RequestParam(required=false) String id) {
		FormData entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = formDataService.get(id);
		}
		if (entity == null){
			entity = new FormData();
		}
		return entity;
	}
	
	@RequiresPermissions("flowable:formData:view")
	@RequestMapping(value = {"list", ""})
	public String list() {
		return "modules/flowable/formDataList";
	}
	
	@RequiresPermissions("flowable:formData:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<FormData> listData(FormData formData, HttpServletRequest request, HttpServletResponse response) {
		Page<FormData> page = formDataService.findPage(new Page<>(request, response), formData);
		return page;
	}

	@RequiresPermissions("flowable:formData:view")
	@RequestMapping(value = "form")
	public String form(FormData formData, Model model) {
		model.addAttribute("formData", formData);
		return "modules/flowable/formDataForm";
	}

	@RequiresPermissions("flowable:formData:edit")
	@RequestMapping(value = "save")
	@ResponseBody
	public String save(FormData formData) {
		formDataService.save(formData);
		return renderResult(Global.TRUE, "保存表单数据信息成功");
	}
	
	@RequiresPermissions("flowable:formData:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(FormData formData) {
		formDataService.delete(formData);
		return renderResult(Global.TRUE, "删除表单数据信息成功");
	}

}