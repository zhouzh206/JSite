package com.jsite.modules.flowable.entity;

import com.jsite.common.persistence.DataEntity;

public class FormUser extends DataEntity<FormUser> {

    private String modelKey;		// 流程模型key
    private String modelVersion;		// 流程模型版本
    private String formId;
    private String userId;

    public String getModelKey() {
        return modelKey;
    }

    public void setModelKey(String modelKey) {
        this.modelKey = modelKey;
    }

    public String getModelVersion() {
        return modelVersion;
    }

    public void setModelVersion(String modelVersion) {
        this.modelVersion = modelVersion;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
